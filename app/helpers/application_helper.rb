module ApplicationHelper
  def flash_class(type)
    case type
     when 'alert'
        "alert alert-warning alert-dismissible"
      when 'error'
        "alert alert-danger alert-dismissible"
      when 'notice'
        "alert alert-success alert-dismissible"
      else
        "alert alert-info alert-dismissable"
    end
  end
end
